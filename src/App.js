import React, {useState, useEffect} from 'react';
import './App.css';
import * as API from './api/api'
import InputTextBox from './components/InputTextBox';
import Contact from './components/Contact';

function App() {

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [showSuccessMessage, setShowSuccessMessage] = useState(false)
  const [showErrorMessage, setShowErrorMessage] = useState(false)
  const [successMessage, setSuccessMessage] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const [contacts, setContacts] = useState([])

  // send the request to the backend to load the contacts on the first load of the page
  useEffect(() => {
    API.listContacts((response) => {
      setContacts(response.data)
    })
  }, [])
  
  // send the request to the backend a return the response to the user (as a success / error message)
  const addContact = () => {
    const contact = {
      name: name,
      email: email,
      phone_number: phoneNumber
    } 

    API.addContact(contact, (response) => {
      if (response.data.error === false) {
        setShowErrorMessage(false)
        setErrorMessage('')
        setSuccessMessage(response.data.message)
        setShowSuccessMessage(true)
        setName('')
        setPhoneNumber('')
        setEmail('')
        setContacts(oldContacts => [...oldContacts, response.data.contact])
      } else {
        setShowErrorMessage(true)
        setErrorMessage(response.data.message)
        setSuccessMessage('')
        setShowSuccessMessage(false)
      }
    })
  }

  // send the reuqest to the backend to delete a contact and return the response to the user (as a success / error message)
  const deleteContact = (contact_uuid) => {
    const uuid = {
      uuid: contact_uuid
    }


    API.deleteContact(uuid, (response) => {
      if (response.data.error === false) {
          setShowErrorMessage(false)
          setErrorMessage('')
          setSuccessMessage(response.data.message)
          setShowSuccessMessage(true)
          setContacts(contacts.filter(contact => contact.uuid !== contact_uuid))
      } else {
        setShowErrorMessage(true)
        setErrorMessage(response.data.message)
        setSuccessMessage('')
        setShowSuccessMessage(false)
      }
    })
  }

  return (
    <div className="App">
        <div className='add-contact-wrapper'>
          <InputTextBox placeholder="Enter the contact's name..." onChange={e => setName(e.target.value)} value={name}/>
          <InputTextBox placeholder="Enter the contact's email..." onChange={e => setEmail(e.target.value)} value={email}/>
          <InputTextBox placeholder="Enter the contact's phone number..." onChange={e => setPhoneNumber(e.target.value)} value={phoneNumber}/>
          <button className="add-contact-button" onClick={addContact}>ADD CONTACT</button>
          <p className='success-message'>{showSuccessMessage && successMessage}</p>
          <p className="error-message">{showErrorMessage && errorMessage}</p>
        </div>

        <div className='contacts-wrapper'>
          {contacts.length === 0 ?
            <div className="no-contacts-added"> No contacts added yet... </div>
            :
            contacts.map((contact, index) => {
              return(
                <Contact 
                  key = {contact.uuid}
                  uuid = {contact.uuid}
                  name = {contact.name}
                  email = {contact.email}
                  phoneNumber = {contact.phone_number}
                  onDelete = {() => deleteContact(contact.uuid)}
                  showSuccessMessage = {setShowSuccessMessage}
                  successMessage = {setSuccessMessage}
                  showErrorMessage={setShowErrorMessage}
                  errorMessage = {setErrorMessage}
                />
              )
            })
          }
        </div>
    </div>
  );
}

export default App;
