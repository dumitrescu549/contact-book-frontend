import url from './host'
import axios from 'axios'
const HOST = url.getHost()

const endpoint = '/contacts'

const addContact = (contact, callback) => {
    axios({
        method: "post",
        url: HOST + endpoint,
        data: contact,
        headers: { "Content-Type": "application/json" },
      })
    .then(callback)
    .catch(function (response) {
        console.log(response);
    });
}

const listContacts = (callback) => {
    axios({
        method: "get",
        url: HOST + endpoint,
        headers: { "Content-Type": "application/json" },
      })
    .then(callback)
    .catch(function (response) {
        console.log(response);
    });
}

const editContact = (contact, callback) => {
    axios({
        method: "put",
        url: HOST + endpoint,
        data: contact,
        headers: { "Content-Type": "application/json" },
      })
    .then(callback)
    .catch(function (response) {
        console.log(response);
    });
}

const deleteContact = (uuid, callback) => {
    axios({
        method: "delete",
        url: HOST + endpoint,
        data: uuid,
        headers: { "Content-Type": "application/json" },
      })
    .then(callback)
    .catch(function (response) {
        console.log(response);
    });
}

export {
    addContact,
    listContacts,
    editContact,
    deleteContact
}