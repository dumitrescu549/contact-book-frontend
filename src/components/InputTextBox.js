import '../App.css'

const InputTextBox = (props) => {
    return (
        <input className="text-input" placeholder={props.placeholder} onChange={props.onChange} value={props.value}/>
    )
}

export default InputTextBox