import '../App.css'
import React, {useState} from 'react';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import DoneIcon from '@mui/icons-material/Done';
import * as API from '../api/api'

const Contact = (props) => {

    const [editing, setEditing] = useState(false)
    const [uuid] = useState(props.uuid)
    const [name, setName] = useState(props.name)
    const [email, setEmail] = useState(props.email)
    const [phoneNumber, setPhoneNumber] = useState(props.phoneNumber)

    const onEdit = () => {
        setEditing(true)
    }

    const onDone = () => {
        const contact = {
            'uuid': uuid,
            'name': name,
            'email': email,
            'phone_number': phoneNumber
        }
        
        API.editContact(contact, (response) => {
            if (response.data.error === false) {
                props.successMessage(response.data.message)
                props.showSuccessMessage(true)
                props.showErrorMessage(false)
                props.errorMessage('')
                setEditing(false)
            } else {
                props.successMessage('')
                props.showSuccessMessage(false)
                props.errorMessage(response.data.message)
                props.showErrorMessage(true)
            }
        })
    }

    return (
        <div className="contact-wrapper">
            <div className="name-delete-edit">
                {!editing && 
                    <div className="delete-edit-element"> 
                        <IconButton onClick={onEdit}>    
                            <EditIcon/>
                        </IconButton>
                    </div>
                }
                {!editing ? 
                    <div className="name-element">{name}</div>
                    :
                    <input className="edit-text-box" value={name} onChange={e => setName(e.target.value)}></input>
                }
                {!editing ?
                    <div className="delete-edit-element">
                        <IconButton onClick={props.onDelete}>
                            <DeleteIcon/>
                        </IconButton>
                    </div>
                    :
                    <div className="delete-edit-element">
                        <IconButton onClick={onDone}>
                            <DoneIcon/>
                        </IconButton>
                    </div>
                }

            </div>

            <div className='phone-email-wrapper'>
                <div className="phone-email">
                    <div className="phone-email-icon">
                        <PhoneIcon />
                    </div>
                    {!editing ?
                        <div className="phone-email-content">
                            <p>{phoneNumber}</p>
                        </div>
                        :
                        <input className="edit-text-box-phone-email" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}></input>
                    }
                </div>

                <div className="phone-email">
                    <div className="phone-email-icon">
                        <EmailIcon />
                    </div>
                    {!editing ?
                        <div className="phone-email-content">
                            <p>{email}</p>
                        </div>
                        :
                        <input className="edit-text-box-phone-email" value={email} onChange={e => setEmail(e.target.value)}></input>
                    }
                </div>
            </div> 
        </div>
    )
}

export default Contact