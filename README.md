# Setting up Node.js and npm
In order to install Node.js and npm, follow this tutorial:
https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

# Pull the project
After setting up Node.js and npm, you need to pull the project on your local machine.

# Run the project
To start the application, just write in your terminal this command(make sure you're in the right folder):
npm start
The application should launch your default browser with the website running.

The URL and PORT which will open the application are: http://localhost:3000 (these can also be modified by you)

Request's won't go through unless the backend application is also running. (https://gitlab.com/dumitrescu549/contact-book-backend)

If you want to change the URL or the PORT, make sure to modify in the backend the CORS_ALLOWED_ORIGINS and add your new URL+PORT, since requests won't go through.
